from application_form_api.views import (ApplicationFormViewSet,
                                        DeliveryViewSet, TransportViewSet)
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'transport', TransportViewSet)
router.register(r'application_form', ApplicationFormViewSet)
router.register(r'delivery', DeliveryViewSet)

urlpatterns = router.urls
