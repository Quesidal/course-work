from application_form.models import ApplicationForm, Delivery, Transport
from rest_framework import serializers


class TransportSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transport
        fields = [
            'car_manufacturer',
            'vehicle_number',
            'fuel_consumption',
        ]


class ApplicationFormSerializer(serializers.ModelSerializer):
    date = serializers.DateField()

    class Meta:
        model = ApplicationForm
        fields = [
            'date',
            'from_place',
            'to_place',
            'load_name',
            'units',
            'load_count',
        ]


class DeliverySerializer(serializers.ModelSerializer):
    class Meta:
        model = Delivery
        fields = [
            'start_datetime',
            'return_date',
            'vehicle_number',
            'application_form',
            'actual_load_count',
            'travelled_distance',
        ]
