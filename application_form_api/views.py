from application_form.models import ApplicationForm, Delivery, Transport
from application_form_api.serializers import (ApplicationFormSerializer,
                                              DeliverySerializer,
                                              TransportSerializer)
from rest_framework.mixins import (CreateModelMixin, DestroyModelMixin,
                                   ListModelMixin, RetrieveModelMixin,
                                   UpdateModelMixin)
from rest_framework.viewsets import GenericViewSet


class MyCrudViewSet(ListModelMixin,
                    RetrieveModelMixin,
                    UpdateModelMixin,
                    CreateModelMixin,
                    DestroyModelMixin,
                    GenericViewSet, ):
    pass


class TransportViewSet(MyCrudViewSet):
    queryset = Transport.objects.all()
    serializer_class = TransportSerializer


class ApplicationFormViewSet(MyCrudViewSet):
    queryset = ApplicationForm.objects.all()
    serializer_class = ApplicationFormSerializer


class DeliveryViewSet(MyCrudViewSet):
    queryset = Delivery.objects.all()
    serializer_class = DeliverySerializer
