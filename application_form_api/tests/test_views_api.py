from application_form.models import Transport
from django.test import TestCase
from django.urls import reverse


class TransportViewSetTestCase(TestCase):
    def test_create(self):
        data = {
            'car_manufacturer': '123',
            'vehicle_number': '12345',
            'fuel_consumption': '12345'
        }

        response = self.client.post(
            path=reverse('application_form_api:transport-list'),
            data=data
        )
        self.assertEqual(response.status_code, 201)
        self.assertEqual(Transport.objects.count(), 1)

    def test_update(self):
        test_car = Transport.objects.create(car_manufacturer='test',
                                            vehicle_number=1234,
                                            fuel_consumption=4)

        update_value = '12345'
        data = {
            'car_manufacturer': update_value,
            'vehicle_number': update_value,
            'fuel_consumption': update_value
        }

        url = reverse('application_form_api:transport-detail',
                      args=[test_car.pk])
        response = self.client.patch(
            path=url,
            data=data,
            format='json',
            content_type='application/json'
        )

        self.assertEqual(response.status_code, 200)

        self.assertEqual(
            Transport.objects.get(pk=test_car.pk).car_manufacturer,
            update_value
        )

    def test_partitial_update(self):
        test_car = Transport.objects.create(car_manufacturer='test',
                                            vehicle_number=1234,
                                            fuel_consumption=4)

        update_value = '12345'
        data = {
            'car_manufacturer': update_value,
            'vehicle_number': update_value,
        }

        url = reverse('application_form_api:transport-detail',
                      args=[test_car.pk])
        response = self.client.patch(
            path=url,
            data=data,
            format='json',
            content_type='application/json'
        )

        self.assertEqual(response.status_code, 200)

        self.assertEqual(
            Transport.objects.get(pk=test_car.pk).car_manufacturer,
            update_value
        )

    def test_delete(self):
        test_car = Transport.objects.create(car_manufacturer='test',
                                            vehicle_number=1234,
                                            fuel_consumption=4)

        url = reverse('application_form_api:transport-detail',
                      args=[test_car.pk])
        response = self.client.delete(path=url)

        self.assertEqual(response.status_code, 204)

        self.assertEqual(Transport.objects.count(), 0)
