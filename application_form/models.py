from django.db import models
from django.urls import reverse


class Transport(models.Model):
    car_manufacturer = models.CharField(max_length=255,
                                        verbose_name="Car manufacturer")
    vehicle_number = models.CharField(max_length=8,
                                      unique=True,
                                      blank=False,
                                      null=False,
                                      verbose_name="Car license number")
    fuel_consumption = models.PositiveSmallIntegerField(
        verbose_name="Car fuel consumption"
    )

    def get_absolute_url(self):
        return reverse('application_form:transport-detail',
                       kwargs={'pk': self.pk})

    def __str__(self):
        return self.vehicle_number


class ApplicationForm(models.Model):
    date = models.DateField(auto_now_add=True,
                            verbose_name="Date")
    from_place = models.CharField(max_length=20,
                                  verbose_name="Place where route start")
    to_place = models.CharField(max_length=20,
                                verbose_name="Place where route end")
    load_name = models.CharField(max_length=255,
                                 verbose_name="Product which delivered")
    units = models.CharField(max_length=255,
                             verbose_name="Units of measurement")
    load_count = models.PositiveIntegerField(
        verbose_name="Count of products to deliver"
    )

    def get_absolute_url(self):
        return reverse('application_form:application-form-detail',
                       kwargs={'pk': self.pk})

    def __str__(self):
        return f'{self.from_place} --- {self.to_place}'


class Delivery(models.Model):
    start_datetime = models.DateTimeField(
        verbose_name="Date and time start delivery"
    )
    return_date = models.DateTimeField(
        verbose_name="Date and time return delivery"
    )
    vehicle_number = models.ForeignKey('Transport',
                                       on_delete=models.CASCADE)
    application_form = models.ForeignKey('ApplicationForm',
                                         on_delete=models.CASCADE)
    actual_load_count = models.PositiveIntegerField(
        verbose_name="Actual count delivered products"
    )
    travelled_distance = models.PositiveIntegerField(
        verbose_name="Length travelled route"
    )

    def get_absolute_url(self):
        return reverse('application_form:delivery-detail',
                       kwargs={'pk': self.pk})
