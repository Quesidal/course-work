# Register your models here.
from application_form.models import ApplicationForm, Delivery, Transport
from django.contrib import admin


@admin.register(Transport)
class AdminTransport(admin.ModelAdmin):
    pass


@admin.register(ApplicationForm)
class AdminApplicationForm(admin.ModelAdmin):
    pass


@admin.register(Delivery)
class AdminDelivery(admin.ModelAdmin):
    pass
