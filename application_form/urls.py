from application_form.views import (ApplicationFormCreateView,
                                    ApplicationFormDeleteView,
                                    ApplicationFormDetailView,
                                    ApplicationFormListView,
                                    ApplicationFormUpdateView,
                                    DeliveryCreateView, DeliveryDeleteView,
                                    DeliveryDetailView, DeliveryListView,
                                    DeliveryUpdateView, MainPageView,
                                    TransportCreateView, TransportDeleteView,
                                    TransportDetailView, TransportListView,
                                    TransportUpdateView)
from django.conf.urls import url

transport_urlpatterns = [
    url(r'^transport/$',
        TransportListView.as_view(),
        name='transport-list'),
    url(r'^transport/(?P<pk>\d+)$',
        TransportDetailView.as_view(),
        name='transport-detail'),
    url(r'^transport/create$',
        TransportCreateView.as_view(),
        name='transport-create'),
    url(r'^transport/(?P<pk>\d+)/update$',
        TransportUpdateView.as_view(),
        name='transport-update'),
    url(r'^transport/(?P<pk>\d+)/delete$',
        TransportDeleteView.as_view(),
        name='transport-delete'),
]

application_form_urlpatterns = [
    url(r'^application_form/$',
        ApplicationFormListView.as_view(),
        name='application-form-list'),
    url(r'^application_form/(?P<pk>\d+)$',
        ApplicationFormDetailView.as_view(),
        name='application-form-detail'),
    url(r'^application_form/create$',
        ApplicationFormCreateView.as_view(),
        name='application-form-create'),
    url(r'^application_form/(?P<pk>\d+)/update$',
        ApplicationFormUpdateView.as_view(),
        name='application-form-update'),
    url(r'^application_form/(?P<pk>\d+)/delete$',
        ApplicationFormDeleteView.as_view(),
        name='application-form-delete'),
]

delivery_urlpatterns = [
    url(r'^delivery/$',
        DeliveryListView.as_view(),
        name='delivery-list'),
    url(r'^delivery/(?P<pk>\d+)$',
        DeliveryDetailView.as_view(),
        name='delivery-detail'),
    url(r'^delivery/create$',
        DeliveryCreateView.as_view(),
        name='delivery-create'),
    url(r'delivery/(?P<pk>\d+)/update$',
        DeliveryUpdateView.as_view(),
        name='delivery-update'),
    url(r'^delivery/(?P<pk>\d+)/delete$',
        DeliveryDeleteView.as_view(),
        name='delivery-delete'),
]

urlpatterns = [
    url(r'^$', MainPageView.as_view(), name='main-page'),

    *transport_urlpatterns,
    *application_form_urlpatterns,
    *delivery_urlpatterns,
]
