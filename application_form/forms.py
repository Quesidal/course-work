from application_form.models import Delivery
from django import forms


class CorrectDataDeliveryForm(forms.ModelForm):
    start_datetime = forms.DateTimeField(
        input_formats=['%Y-%m-%dT%H:%M'],
        widget=forms.DateTimeInput(
            attrs={
                'type': 'datetime-local',
                'class': 'form-control'
            },
            format='%Y-%m-%dT%H:%M')
    )
    return_date = forms.DateTimeField(
        input_formats=['%Y-%m-%dT%H:%M'],
        widget=forms.DateTimeInput(
            attrs={
                'type': 'datetime-local',
                'class': 'form-control'
            },
            format='%Y-%m-%dT%H:%M')
    )

    class Meta:
        model = Delivery
        fields = ['start_datetime',
                  'return_date',
                  'vehicle_number',
                  'application_form',
                  'actual_load_count',
                  'travelled_distance']
