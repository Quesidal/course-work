from application_form.forms import CorrectDataDeliveryForm
from application_form.models import ApplicationForm, Delivery, Transport
from django.views.generic import (CreateView, DeleteView, DetailView, ListView,
                                  TemplateView, UpdateView)


class MainPageView(TemplateView):
    template_name = 'application_form/index.html'


class TransportListView(ListView):
    model = Transport
    context_object_name = 'transports'


class TransportDetailView(DetailView):
    model = Transport


class TransportCreateView(CreateView):
    model = Transport
    fields = ['car_manufacturer',
              'vehicle_number',
              'fuel_consumption']
    success_url = '/transport'


class TransportUpdateView(UpdateView):
    model = Transport
    fields = ['car_manufacturer',
              'vehicle_number',
              'fuel_consumption']


class TransportDeleteView(DeleteView):
    model = Transport
    success_url = '/transport'


class ApplicationFormDetailView(DetailView):
    model = ApplicationForm


class ApplicationFormListView(ListView):
    model = ApplicationForm
    context_object_name = 'application_forms'


class ApplicationFormCreateView(CreateView):
    model = ApplicationForm
    fields = ['from_place',
              'to_place',
              'load_name',
              'units',
              'load_count']
    success_url = '/application_form'


class ApplicationFormUpdateView(UpdateView):
    model = ApplicationForm
    fields = ['from_place',
              'to_place',
              'load_name',
              'units',
              'load_count']
    success_url = '/application_form'


class ApplicationFormDeleteView(DeleteView):
    model = ApplicationForm
    success_url = '/application_form'


class DeliveryDetailView(DetailView):
    model = Delivery


class DeliveryListView(ListView):
    model = Delivery
    context_object_name = 'deliveries'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['application_forms'] = ApplicationForm.objects.all()
        context['vehicles'] = Transport.objects.all()
        return context


class DeliveryCreateView(CreateView):
    model = Delivery
    form_class = CorrectDataDeliveryForm
    success_url = '/delivery'


class DeliveryUpdateView(UpdateView):
    model = Delivery
    fields = ['start_datetime',
              'return_date',
              'vehicle_number',
              'application_form',
              'actual_load_count',
              'travelled_distance']
    success_url = '/delivery'


class DeliveryDeleteView(DeleteView):
    model = Delivery
    success_url = '/delivery'
