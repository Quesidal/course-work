from application_form.models import ApplicationForm, Delivery, Transport
from django.test import TestCase
from django.urls import reverse


class TransportCRUDTestCase(TestCase):
    def test_transport_add(self):
        data = {
            'car_manufacturer': '123',
            'vehicle_number': '12345',
            'fuel_consumption': '12345'
        }

        response = self.client.post(
            path=reverse('application_form:transport-create'),
            data=data
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Transport.objects.count(), 1)

    def test_transport_update(self):
        my_car = Transport.objects.create(
            car_manufacturer='12345',
            vehicle_number='12345',
            fuel_consumption=10
        )

        data = {
            'car_manufacturer': 'test',
            'vehicle_number': 'test',
            'fuel_consumption': 15
        }

        response = self.client.post(
            path=reverse('application_form:transport-update',
                         args=[my_car.pk]),
            data=data
        )

        updated_car = Transport.objects.get(pk=my_car.pk)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(updated_car.car_manufacturer, 'test')
        self.assertEqual(updated_car.vehicle_number, 'test')
        self.assertEqual(updated_car.fuel_consumption, 15)

    def test_transport_delete(self):
        my_car = Transport.objects.create(
            car_manufacturer='12345',
            vehicle_number='12345',
            fuel_consumption=10
        )

        response = self.client.post(
            path=reverse('application_form:transport-delete',
                         args=[my_car.pk])
        )

        self.assertEqual(response.status_code, 302)
        self.assertEqual(Transport.objects.count(), 0)


class ApplicationFormCRUDTestCase(TestCase):
    def test_application_form_add(self):
        data = {
            'from_place': '12345',
            'to_place': '12345',
            'load_name': '12345',
            'units': '12345',
            'load_count': 12345
        }

        response = self.client.post(
            path=reverse('application_form:application-form-create'),
            data=data
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(ApplicationForm.objects.count(), 1)

    def test_application_form_update(self):
        my_form = ApplicationForm.objects.create(
            from_place='12345',
            to_place='12345',
            load_name='12345',
            units='12345',
            load_count=12345
        )

        data = {
            'from_place': 'test',
            'to_place': 'test',
            'load_name': 'test',
            'units': 'test',
            'load_count': 0
        }

        response = self.client.post(
            path=reverse('application_form:application-form-update',
                         args=[my_form.pk]),
            data=data
        )

        updated_form = ApplicationForm.objects.get(pk=my_form.pk)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(updated_form.from_place, 'test')
        self.assertEqual(updated_form.to_place, 'test')
        self.assertEqual(updated_form.load_name, 'test')
        self.assertEqual(updated_form.units, 'test')
        self.assertEqual(updated_form.load_count, 0)

    def test_application_form_delete(self):
        my_form = ApplicationForm.objects.create(
            from_place='12345',
            to_place='12345',
            load_name='12345',
            units='12345',
            load_count=12345
        )

        response = self.client.post(
            path=reverse('application_form:application-form-delete',
                         args=[my_form.pk])
        )

        self.assertEqual(response.status_code, 302)
        self.assertEqual(ApplicationForm.objects.count(), 0)


class DeliveryCRUDTestCase(TestCase):

    def setUp(self):
        self.my_car = Transport.objects.create(
            car_manufacturer='12345',
            vehicle_number='12345',
            fuel_consumption=10
        )

        self.my_form = ApplicationForm.objects.create(
            from_place='12345',
            to_place='12345',
            load_name='12345',
            units='12345',
            load_count=12345
        )

    def test_delivery_add(self):
        data = {
            'start_datetime': '2019-10-10T12:45',
            'return_date': '2019-10-10T12:45',
            'vehicle_number': self.my_car.pk,
            'application_form': self.my_form.pk,
            'actual_load_count': 12345,
            'travelled_distance': 12345
        }

        response = self.client.post(
            path=reverse('application_form:delivery-create'),
            data=data
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Delivery.objects.count(), 1)

    def test_delivery_update(self):
        my_delivery = Delivery.objects.create(
            start_datetime='2019-10-10 12:45',
            return_date='2019-10-10 12:45',
            vehicle_number=self.my_car,
            application_form=self.my_form,
            actual_load_count=12345,
            travelled_distance=12345
        )

        data = {
            'start_datetime': '2019-10-10 12:45',
            'return_date': '2019-10-10 12:45',
            'vehicle_number': self.my_car.pk,
            'application_form': self.my_form.pk,
            'actual_load_count': 0,
            'travelled_distance': 0
        }

        response = self.client.post(
            path=reverse('application_form:delivery-update',
                         args=[my_delivery.pk]),
            data=data
        )

        updated_delivery = Delivery.objects.get(pk=my_delivery.pk)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(updated_delivery.actual_load_count, 0)
        self.assertEqual(updated_delivery.travelled_distance, 0)

    def test_delivery_delete(self):
        my_delivery = Delivery.objects.create(
            start_datetime='2019-10-10 12:45',
            return_date='2019-10-10 12:45',
            vehicle_number=self.my_car,
            application_form=self.my_form,
            actual_load_count=12345,
            travelled_distance=12345
        )

        response = self.client.post(
            path=reverse('application_form:delivery-delete',
                         args=[my_delivery.pk])
        )

        self.assertEqual(response.status_code, 302)
        self.assertEqual(Delivery.objects.count(), 0)
